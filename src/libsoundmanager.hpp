/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBSOUNDMANAGER_H
#define LIBSOUNDMANAGER_H
#include <vector>
#include <map>
#include <string>
#include <json-c/json.h>
#include <systemd/sd-event.h>
extern "C"
{
#include <afb/afb-wsj1.h>
#include <afb/afb-ws-client.h>
}

class Soundmanager
{
public:
    Soundmanager();
    ~Soundmanager();
    Soundmanager(const Soundmanager &) = delete;
    Soundmanager &operator=(const Soundmanager &) = delete;
    int init(int port, const std::string& token);
    void registerCallback(
        void (*event_cb)(const std::string& event, struct json_object* event_contents),
        void (*reply_cb)(struct json_object* reply_contents),
        void (*hangup_cb)(void) = nullptr);
    struct sd_event* getEventLoop();

    /* Method */
    int registerSource(const std::string& audio_role);
    int connect(int source_id, int sink_id);
    int connect(int source_id, const std::string& sink_name);
    int disconnect(int connection_id);
    int ackSetSourceState(int handle, int error = 0);
    int getListMainSources();
    int getListMainSinks();
    int getListMainConnections();

    int streamOpen(const std::string& audio_role, int endpoint_id);
    int streamOpen(const std::string& audio_role, const std::string& endpoint_id = "default");
    int streamClose(int stream_id);
    int setStreamState(int stream_id, int mute_state = 0); // 0 is unmute, 1 is mute

    int call(const std::string& verb, struct json_object* arg);
    int call(const char* verb, struct json_object* arg);
    int subscribe(const std::string& event_name);
    int unsubscribe(const std::string& event_name);

private:
    int initEvent();
    int initializeWebsocket();

    void (*onEvent)(const std::string& event, struct json_object* event_contents);
    void (*onReply)(struct json_object* reply);
    void (*onHangup)(void);

    struct afb_wsj1* sp_websock;
    struct afb_wsj1_itf minterface;
    sd_event* mploop;
    int mport;
    std::string mtoken;
    std::vector<int> msourceIDs;

public:
    /* Don't use. Internal only */
    void _onHangup(void *closure, struct afb_wsj1 *wsj);
    void _onCall(void *closure, const char *api, const char *verb, struct afb_wsj1_msg *msg);
    void _onEvent(void *closure, const char *event, struct afb_wsj1_msg *msg);
    void _onReply(void *closure, struct afb_wsj1_msg *msg);
};

#endif /* LIBSOUNDMANAGER_H */
